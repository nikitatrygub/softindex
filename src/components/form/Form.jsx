import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import cuid from "cuid";
import { setUser } from "../../LocalDB";
import { parsePhoneNumberFromString } from "libphonenumber-js";
import InputMask from "react-input-mask";

class Form extends Component {
  state = {
    firstName: "",
    lastName: "",
    phone: "+38",
    age: "",
    users: []
  };

  pushUser = () => {
    let userId = cuid();
    let user = {
      id: userId,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      phone: this.state.phone,
      age: parseFloat(this.state.age)
    };

    const users = [...this.state.users, user];
    this.props.setUsers(users);
    this.setState({ users });

    const serializeUser = JSON.stringify(users);
    setUser(serializeUser);

    this.setState({
      firstName: "",
      lastName: "",
      phone: "",
      age: ""
    });
  };

  componentDidUpdate(prevState) {
    if (this.props.users !== prevState.users) {
      this.setState({ users: this.props.users });
    }
  }

  isValidPhone = phone => {
    const phoneNumber = parsePhoneNumberFromString(`Phone: ${phone} `, "UA");

    if (phoneNumber) {
      return phoneNumber.isValid() === true;
    }
    return false;
  };

  render() {
    const { firstName, lastName, phone, age } = this.state;

    const isValidFirstName = firstName.length > 0,
      isValidLastName = lastName.length > 0,
      isValidPhone = this.isValidPhone(phone),
      isValidAge = age.length > 0,
      isValidForm =
        isValidFirstName && isValidLastName && isValidPhone && isValidAge;
    return (
      <div className="container form">
        <div className="form__title">
          Please , fill the form and click <span>Save button</span>
        </div>
        <div className="input-wrapper">
          <div className="row">
            <div className="col-12 col-md-6">
              <TextField
                fullWidth
                error={this.state.firstNameError}
                label={
                  this.state.firstNameError
                    ? "First name is required field"
                    : "First name"
                }
                variant="outlined"
                color="primary"
                margin="normal"
                value={firstName}
                onBlur={() =>
                  !isValidFirstName
                    ? this.setState({
                        firstNameError: true
                      })
                    : null
                }
                onChange={({ target: { value } }) =>
                  this.setState({ firstName: value, firstNameError: false })
                }
              />
            </div>
            <div className="col-12 col-md-6">
              <TextField
                fullWidth
                error={this.state.lastNameError}
                label={
                  this.state.lastNameError
                    ? "Last name is required field"
                    : "Last name"
                }
                variant="outlined"
                color="primary"
                margin="normal"
                value={lastName}
                onChange={({ target: { value } }) =>
                  this.setState({ lastName: value, lastNameError: false })
                }
                onBlur={() =>
                  !isValidLastName
                    ? this.setState({
                        lastNameError: true
                      })
                    : null
                }
              />
            </div>
            <div className="col-12 col-md-6">
              <InputMask
                mask="+38(999) 999 99 99"
                value={phone}
                onChange={({ target: { value } }) =>
                  this.setState({ phone: value, phoneError: false })
                }
                onBlur={() =>
                  !isValidPhone
                    ? this.setState({
                        phoneError: true
                      })
                    : null
                }
              >
                {() => (
                  <TextField
                    fullWidth
                    error={this.state.phoneError}
                    label={
                      this.state.phoneError
                        ? "Incorrect phone number"
                        : "Phone"
                    }
                    variant="outlined"
                    color="primary"
                    margin="normal"
                  />
                )}
              </InputMask>
            </div>
            <div className="col-12 col-md-6">
              <TextField
                fullWidth
                error={this.state.ageError}
                label={
                  this.state.ageError ? "Age is required field" : "Age"
                }
                variant="outlined"
                color="primary"
                type="number"
                margin="normal"
                value={age}
                onChange={({ target: { value } }) =>
                  this.setState({ age: value, ageError: false })
                }
                onBlur={() =>
                  !isValidAge
                    ? this.setState({
                        ageError: true
                      })
                    : null
                }
              />
            </div>
          </div>
          <Button
            className="form__submit-btn"
            disabled={!isValidForm}
            color="secondary"
            variant="contained"
            onClick={this.pushUser}
          >
            Save
          </Button>
        </div>
      </div>
    );
  }
}

export default Form;
