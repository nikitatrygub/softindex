import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";

export const Preloader = () => <CircularProgress disableShrink />;
