import React, { Component, Fragment } from "react";

class Tabs extends Component {
  state = {
    currentTab: "form"
  };
  changeTab = tab => {
    this.setState({ currentTab: tab });
  };
  render() {
    const { currentTab } = this.state;
    return (
      <Fragment>
        <div className="tabs-header container-fluid">
          <div className="row">
            <div className="col-6" style={{ padding: 0 }}>
              <button
                className={`tabs-header__button ${
                  currentTab === "form" ? "active" : ""
                }`}
                fullWidth
                color="primary"
                onClick={() => this.changeTab("form")}
              >
                Form
              </button>
            </div>
            <div className="col-6" style={{ padding: 0 }}>
              <button
                className={`tabs-header__button ${
                  currentTab === "table" ? "active" : ""
                }`}
                fullWidth
                color="primary"
                onClick={() => this.changeTab("table")}
              >
                Table
              </button>
            </div>
          </div>
        </div>
        {this.props.children({ currentTab: this.state.currentTab })}
      </Fragment>
    );
  }
}

export default Tabs;
