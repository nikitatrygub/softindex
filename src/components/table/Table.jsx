import React, { Component } from "react";
// import Checkbox from "@material-ui/core/Checkbox";
import orderBy from "lodash.orderby";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import { withStyles } from "@material-ui/core/styles";
import Info from "@material-ui/icons/Info";
import ArrowDropDown from "@material-ui/icons/ArrowDropDown";
import ArrowDropUp from "@material-ui/icons/ArrowDropUp";

class Table extends Component {
  state = {
    orderedBy: "firstName",
    order: "asc"
  };

  sortByOrder = orderedBy => {
    this.setState({
      orderedBy: orderedBy,
      order: this.state.order === "asc" ? "desc" : "asc"
    });
  };

  pushUserForDelete = user => {
    this.setState({ selected: [...this.state.selected, user] });
  };

  render() {
    const { orderedBy, order } = this.state;
    const users = this.props.users;

    const sortDataTable = (orderedBy, order) => {
      return orderBy(users, [orderedBy], [order]);
    };

    const renderArrow = order =>
      order === "asc" ? <ArrowDropUp /> : <ArrowDropDown />;

    return (
      <div className="container">
        <div className="table">
          <div className="table-wrap">
            <div className="table-header">
              <ul className="table-header__list">
                <li>
                  <span
                    className="table-header__button"
                    onClick={() => this.sortByOrder("firstName")}
                  >
                    {orderedBy === "firstName" && renderArrow(order)}
                    <span className="table-header__button--text">
                      First name
                    </span>
                  </span>
                </li>
                <li>
                  <span className="table-header__button" onClick={() => this.sortByOrder("lastName")}>
                    {orderedBy === "lastName" && renderArrow(order)}
                    <span className="table-header__button--text">
                    Last name
                    </span>
                  </span>
                </li>
                <li>
                  <span className="table-header__button" onClick={() => this.sortByOrder("phone")}>
                    {orderedBy === "phone" && renderArrow(order)}
                    <span className="table-header__button--text">
                    Phone
                    </span>
                  </span>
                </li>
                <li>
                  <span className="table-header__button" onClick={() => this.sortByOrder("age")}>
                    {orderedBy === "age" && renderArrow(order)}
                    <span className="table-header__button--text">
                    age
                    </span>
                  </span>
                </li>
                <li />
              </ul>
            </div>
            <div className="table-body">
              {users.length === 0 && (
                <div className="table-body__empty">
                  <Info />
                  <span>Add your first user!</span>
                </div>
              )}
              {sortDataTable(orderedBy, order).map((user, index) => (
                <User
                  user={user}
                  key={index}
                  deleteUser={this.props.deleteUser}
                />
              ))}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const styles = theme => ({
  button: {
    margin: theme.spacing.unit
  },
  input: {
    display: "none"
  }
});

class _User extends Component {
  state = {
    checked: null
  };
  render() {
    const { user, classes, deleteUser } = this.props;
    return (
      <ul className="table-body__list">
        <li>{user.firstName}</li>
        <li>{user.lastName}</li>
        <li>{user.phone}</li>
        <li>{user.age}</li>
        <li>
          <IconButton
            className={classes.button}
            aria-label="Delete"
            color="secondary"
            onClick={() => deleteUser([user])}
          >
            <DeleteIcon />
          </IconButton>
        </li>
      </ul>
    );
  }
}

const User = withStyles(styles)(_User);

export default Table;
