import React, { Fragment } from "react";

const Layout = ({ children }) => {
  return (
    <Fragment>
      <div className="header">
        <span className="header__title">SoftIndex form</span>
      </div>
      {children}
    </Fragment>
  );
};

export default Layout;
