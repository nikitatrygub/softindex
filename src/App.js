import React, { Component } from "react";
import withTheme from "./withTheme";
import Layout from "./components/layout/Layout";
import Form from "./components/form/Form";
import Table from "./components/table/Table";
import difference from "lodash.difference";
import { setUser, getUsers } from "./LocalDB";
import { Preloader } from "./components/Preloader";

class App extends Component {
  state = {
    users: null,
    loading: true
  };

  setUsers = users => {
    this.setState({ users: users });
  };

  //Delete one user method
  deleteUser = user => {
    const users = this.state.users; //get All users from global state
    const diffUsers = difference(users, user); //Diff users array for clean
    this.setState({ users: diffUsers }); //Update global state users
    let serializeUsers = JSON.stringify(diffUsers);
    setUser(serializeUsers); //Update local db
  };

  componentDidMount() {
    const users = getUsers();
    const serializeUsers = JSON.parse(users);
    setTimeout(() => {
      if (users) {
        this.setState({ users: serializeUsers, loading: false });
      } else {
        this.setState({ users: [], loading: false });
      }
    }, 500);
  }

  render() {
    const { users, loading } = this.state;
    return (
      <Layout>
        <Form setUsers={this.setUsers} users={users} />
        {loading && (
          <div className="loading">
            <Preloader />
          </div>
        )}
        {users && <Table users={users} deleteUser={this.deleteUser} />}
      </Layout>
    );
  }
}

export default withTheme(App);
