// I'm using localStorage as a local DB
const localDB = localStorage;
const getUsers = () => localDB.getItem("users");
const setUser = user => localDB.setItem("users", user);

export { getUsers, setUser };
